<?php

namespace Database\Seeders;

use App\Models\Device;
use App\Models\Rfid;
use App\Models\SecretKey;
use App\Models\User;
use App\Models\WaktuOperasional;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dev = User::create([
            'username' => 'developer',
            'nama' => 'Muhammad Iqbal',
            'password' => Hash::make('secret'),
            'device_id' => 1,
            'nik' => 123
        ]);

        $user = User::create([
            'username' => 'admin',
            'nama' => 'SMP 23 Depok',
            'password' => Hash::make('smpdepok'),
            'device_id' => 1,
            'nik' => 123
        ]);


        $permissions = [
            'user-access', 'user-edit', 'user-create', 'user-delete',
            'permission-access', 'permission-edit', 'permission-create', 'permission-delete',
            'role-access', 'role-edit', 'role-create', 'role-delete', 'kelas-access', 'kelas-create', 'kelas-edit', 'kelas-delete', 'siswa-access', 'siswa-create', 'siswa-edit', 'siswa-delete', 'holiday-access', 'holiday-create', 'holiday-edit', 'holiday-delete', 'dump-user-access', 'dump-siswa-access', 'device-access', 'device-create', 'device-edit', 'device-delete', 'rfid-access', 'history-access', 'absensi-siswa-access', 'absensi-siswa-edit', 'absensi-staff-access', 'absensi-staff-edit', 'setting-access', 'data-master-access', 'data-alat-access', 'absensi-access', 'access-user', 'report-siswa-access', 'report-staff-access',
        ];

        $perm = [];

        foreach ($permissions as $permission) {
            $perm[] =  Permission::create([
                'name' => $permission,
                'guard_name' => 'web'
            ]);
        }

        $me = Role::create([
            'name' => 'Me',
            'guard_name' => 'web'
        ]);

        $sup = Role::create([
            'name' => 'Super Admin',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'Admin',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'Guru',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'Staff',
            'guard_name' => 'web'
        ]);

        $me->syncPermissions($perm);
        $dev->assignRole('Me');
        $sup->syncPermissions($perm);
        $user->assignRole('Super Admin');

        SecretKey::create([
            'key' => 'smp23depok2022'
        ]);

        SecretKey::create([
            'key' => 1
        ]);

        SecretKey::create([
            'key' => 1
        ]);

        SecretKey::create([
            'key' => 1
        ]);

        SecretKey::create([
            'key' => 'Message From SMP 23 Depok
            Nama : $nama
            Kelas : $kelas
            Ket : $keterangan
            Waktu : $tanggal $waktu'
        ]);

        SecretKey::create([
            'key' => 'Message From SMP 23 Depok
            Nama : $nama
            Jabatan : $jabatan
            Ket : $keterangan
            Waktu : $tanggal $waktu'
        ]);

        WaktuOperasional::create([
            'waktu_masuk' => '07:00 - 08:00',
            'waktu_keluar' => '15:00 - 16:00',
            'telat' => '08:15',
        ]);

        WaktuOperasional::create([
            'waktu_masuk' => '07:00 - 08:00',
            'waktu_keluar' => '15:00 - 16:00',
            'telat' => '08:15',
        ]);

        WaktuOperasional::create([
            'waktu_masuk' => '08:30',
            'waktu_keluar' => '14:30',
        ]);

        Device::create([
            'nama' => 'Device 1'
        ]);

        Device::create([
            'nama' => 'Device 2'
        ]);

        Rfid::create([
            'device_id' => 1,
            'rfid' => '123456',
            'status' => 1
        ]);
    }
}
