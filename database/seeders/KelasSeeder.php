<?php

namespace Database\Seeders;

use App\Models\Kelas;
use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kelas = ['VII A', 'VII B', 'VII C'];

        foreach ($kelas as $kls) {
            Kelas::create(['nama' => $kls]);
        }
    }
}
