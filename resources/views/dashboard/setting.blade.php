@extends('layouts.master', ['title' => 'Setting'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('setting.update.waktu', $waktu->id) }}" method="post">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-md-5 text-center">
                            <label for="waktu_masuk">Waktu Masuk Siswa</label>
                        </div>
                        <div class="col-md-5 text-center">
                            <label for="waktu_keluar">Waktu Keluar Siswa</label>
                        </div>
                        <div class="col-md-2 text-center">
                            <label for="telat_siswa">Batas Waktu Telat</label>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="time" name="waktu_awal_masuk" id="waktu_awal_masuk" class="form-control text-center" value="{{ $masuk[0] ?? '-' }}">
                                </div>
                                <div class="col-md-6">
                                    <input type="time" name="waktu_akhir_masuk" id="waktu_akhir_masuk" class="form-control text-center" value="{{ $masuk[1] ?? '-' }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="time" name="waktu_awal_keluar" id="waktu_awal_keluar" class="form-control text-center" value="{{ $keluar[0] ?? '-' }}">
                                </div>

                                <div class="col-md-6">
                                    <input type="time" name="waktu_akhir_keluar" id="waktu_akhir_keluar" class="form-control text-center" value="{{ $keluar[1] ?? '-' }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="time" name="telat_siswa" id="telat_siswa" class="form-control text-center" value="{{ $waktu->telat ?? '-' }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 text-center">
                            <label for="waktu_masuk_staff">Waktu Masuk Staff</label>
                        </div>
                        <div class="col-md-5 text-center">
                            <label for="waktu_keluar_staff">Waktu Keluar Staff</label>
                        </div>
                        <div class="col-md-2 text-center">
                            <label for="telat_staff">Batas Waktu Telat</label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="time" name="waktu_awal_masuk_staff" id="waktu_awal_masuk_staff" class="form-control text-center" value="{{ $masukStaff[0] ?? '-' }}">
                                </div>
                                <div class="col-md-6">
                                    <input type="time" name="waktu_akhir_masuk_staff" id="waktu_akhir_masuk_staff" class="form-control text-center" value="{{ $masukStaff[1] ?? '-' }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="time" name="waktu_awal_keluar_staff" id="waktu_awal_keluar_staff" class="form-control text-center" value="{{ $keluarStaff[0] ?? '-' }}">
                                </div>

                                <div class="col-md-6">
                                    <input type="time" name="waktu_akhir_keluar_staff" id="waktu_akhir_keluar_staff" class="form-control text-center" value="{{ $keluarStaff[1] ?? '-' }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="time" name="telat_staff" id="telat_staff" class="form-control text-center" value="{{ $waktuStaff->telat ?? '-' }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 text-center">
                            <label for="waktu_ijin_pulang">Waktu ijin Pulang Lebih Awal</label>
                        </div>
                        <div class="col-md-7 text-center">
                        </div>

                        <div class="col-md-5">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="time" name="waktu_awal_ijin" id="waktu_awal_ijin" class="form-control text-center" value="{{ $ijinPulang->waktu_masuk ?? '-' }}">
                                </div>
                                <div class="col-md-6">
                                    <input type="time" name="waktu_akhir_ijin" id="waktu_akhir_ijin" class="form-control text-center" value="{{ $ijinPulang->waktu_keluar ?? '-' }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Set Waktu</button>
                            </div>
                        </div>
                    </div>
                </form>

                <form action="{{ route('setting.update.pesan') }}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group d-flex">
                                <div class="form-group-check mr-3">
                                    <input type="checkbox" name="wa_siswa" id="wa_siswa" {{ $wasiswa->key == 1 ? 'checked' : '' }}>
                                    <label for="wa_siswa">Wa Siswa</label>
                                </div>
                                <div class="form-group-check">
                                    <input type="checkbox" name="wa_ortu" id="wa_ortu" {{ $waortu->key == 1 ? 'checked' : '' }}>
                                    <label for="wa_ortu">Wa Ortu</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pesan">Pesan Siswa / Ortu</label>
                                <textarea name="pesan_siswa" id="pesan_siswa" cols="30" rows="5" class="form-control">{{ $pesansiswa->key }}</textarea>
                            </div>
                            <input type="hidden" name="pesansiswa_id" value="{{ $pesansiswa->id }}">
                        </div>

                        <div class="col-md-6">
                            <div class="form-group d-flex">
                                <div class="form-group-check mr-3">
                                    <input type="checkbox" name="wa_staff" id="wa_staff" {{ $wastaff->key == 1 ? 'checked' : '' }}>
                                    <label for="wa_staff">Wa Staff</label>
                                </div>
                                <div class="form-group-check mr-3">
                                    <input type="checkbox" name="wa_pimpinan_1" id="wa_pimpinan_1" {{ $wapimsatu->key == 1 ? 'checked' : '' }}>
                                    <label for="wa_pimpinan_1">Wa Pimpinan 1</label>
                                </div>
                                <div class="form-group-check mr-3">
                                    <input type="checkbox" name="wa_pimpinan_2" id="wa_pimpinan_2" {{ $wapimdua->key == 1 ? 'checked' : '' }}>
                                    <label for="wa_pimpinan_2">Wa Pimpinan 2</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="pimpinan_1">No Pimpinan 1</label>
                                    <input type="number" name="no_pimpinan_1" id="no_pimpinan_1" class="form-control" value="{{ $nopimsatu->key }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="pimpinan_2">No Pimpinan 2</label>
                                    <input type="number" name="no_pimpinan_2" id="no_pimpinan_2" class="form-control" value="{{ $nopimdua->key }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pesan">Pesan Staff</label>
                                <textarea name="pesan_staff" id="pesan_staff" cols="30" rows="5" class="form-control">{{ $pesanstaff->key }}</textarea>
                            </div>
                            <input type="hidden" name="pesanstaff_id" value="{{ $pesanstaff->id }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="url">Url Bot Wa</label>
                        <input type="text" name="url" id="url" class="form-control" value="{{ $url->key }}">

                        <input type="hidden" name="url_id" value="{{ $url->id }}">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update Pesan</button>
                    </div>
                </form>

                <div class="row mt-3">
                    <div class="col-md-12">
                        <div class="card card-stats card-info card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-10 col-stats">
                                        <div class="numbers">
                                            <h4 class="card-title">Keyword</h4>
                                            <p class="card-category">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col">Nama</div>
                                                    <div class="col">$nama</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">Kelas</div>
                                                    <div class="col">$kelas</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">Jabatan</div>
                                                    <div class="col">$jabatan</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">Keterangan</div>
                                                    <div class="col">$keterangan</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">Waktu</div>
                                                    <div class="col">$waktu</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">Tanggal</div>
                                                    <div class="col">$tanggal</div>
                                                </div>
                                            </div>

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="card card-stats card-success card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-2">
                                        <div class="icon-big text-center">
                                            <i class="fas fa-exclamation-triangle"></i>
                                        </div>
                                    </div>
                                    <div class="col-10 col-stats">
                                        <div class="numbers">
                                            <h4 class="card-title">Secret Key</h4>
                                            <p class="card-category"><i class="fas fa-key"></i>
                                                {{ $secretKey->key }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (auth()->user()->id == 1)
                    <div class="col-sm-12 col-md-12">
                        <div class="card card-stats card-warning card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-2">
                                        <div class="icon-big text-center">
                                            <i class="fas fa-link"></i>
                                        </div>
                                    </div>
                                    <div class="col-10 col-stats">
                                        <div class="numbers">
                                            <h4 class="card-title">Url Mode Device</h4>
                                            <p class="card-category">
                                            <div>
                                                <i class="fab fa-chrome"></i>
                                                {{ url()->to('/') }}/api/getmode?key={{ $secretKey->key }}&iddev=XXX
                                            </div>
                                            <div>
                                                <i class="fab fa-chrome"></i>
                                                {{ url()->to('/') }}/api/getmodejson?key={{ $secretKey->key }}&iddev=XXX
                                            </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="card card-stats card-info card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-2">
                                        <div class="icon-big text-center">
                                            <i class="fas fa-link"></i>
                                        </div>
                                    </div>
                                    <div class="col-10 col-stats">
                                        <div class="numbers">
                                            <h4 class="card-title">Url Add Rfid Card</h4>
                                            <p class="card-category">
                                            <div>
                                                <i class="fab fa-chrome"></i>
                                                {{ url()->to('/') }}/api/addcard?key={{ $secretKey->key }}&iddev=XXX
                                            </div>
                                            <div>
                                                <i class="fab fa-chrome"></i>
                                                {{ url()->to('/') }}/api/addcardjson?key={{ $secretKey->key }}&iddev=XXX
                                            </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="card card-stats card-secondary card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-2">
                                        <div class="icon-big text-center">
                                            <i class="fas fa-link"></i>
                                        </div>
                                    </div>
                                    <div class="col-10 col-stats">
                                        <div class="numbers">
                                            <h4 class="card-title">Url Absensi</h4>
                                            <p class="card-category">
                                            <div>
                                                <i class="fab fa-chrome"></i>
                                                {{ url()->to('/') }}/api/absensi?key={{ $secretKey->key }}&iddev=XXX
                                            </div>
                                            <div>
                                                <i class="fab fa-chrome"></i>
                                                {{ url()->to('/') }}/api/absensijson?key={{ $secretKey->key }}&iddev=XXX
                                            </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $(document).ready(function() {
        $("#wa_siswa").on('click', function() {
            let status = 0;

            if ($(this).is(':checked')) {
                status = 1;
            } else {
                status = 0;
            }

            $.ajax({
                url: '/setting/status',
                type: 'GET',
                data: {
                    id: '{{ $wasiswa->id }}',
                    status: status
                },
                success: function(response) {
                    swal("Selamat!", response.message, {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });
                }
            })
        })

        $("#wa_ortu").on('click', function() {
            let status = 0;

            if ($(this).is(':checked')) {
                status = 1;
            } else {
                status = 0;
            }

            $.ajax({
                url: '/setting/status',
                type: 'GET',
                data: {
                    id: '{{ $waortu->id }}',
                    status: status
                },
                success: function(response) {
                    swal("Selamat!", response.message, {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });
                }
            })
        })

        $("#wa_staff").on('click', function() {
            let status = 0;

            if ($(this).is(':checked')) {
                status = 1;
            } else {
                status = 0;
            }

            $.ajax({
                url: '/setting/status',
                type: 'GET',
                data: {
                    id: '{{ $wastaff->id }}',
                    status: status
                },
                success: function(response) {
                    swal("Selamat!", response.message, {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });
                }
            })
        })

        $("#wa_pimpinan_1").on('click', function() {
            let status = 0;

            if ($(this).is(':checked')) {
                status = 1;
            } else {
                status = 0;
            }

            $.ajax({
                url: '/setting/status',
                type: 'GET',
                data: {
                    id: '{{ $wapimsatu->id }}',
                    status: status
                },
                success: function(response) {
                    swal("Selamat!", response.message, {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });
                }
            })
        })

        $("#wa_pimpinan_2").on('click', function() {
            let status = 0;

            if ($(this).is(':checked')) {
                status = 1;
            } else {
                status = 0;
            }

            $.ajax({
                url: '/setting/status',
                type: 'GET',
                data: {
                    id: '{{ $wapimdua->id }}',
                    status: status
                },
                success: function(response) {
                    swal("Selamat!", response.message, {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });
                }
            })
        })
    })
</script>
@endpush