<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Absensi;
use App\Models\AbsensiStaff;
use App\Models\Device;
use App\Models\History;
use App\Models\Jadwal;
use App\Models\Rfid;
use App\Models\SecretKey;
use App\Models\Siswa;
use App\Models\User;
use App\Models\WaktuOperasional;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

date_default_timezone_set("asia/jakarta");

class ApiController extends Controller
{
    public function index()
    {
        return 'REST API for Device';
    }

    public function getMode(Request $request)
    {
        if ($request->key && $request->iddev) {
            $cekKey = SecretKey::find(1);

            if ($cekKey->key == $request->key) {
                $device = Device::find($request->iddev);

                if ($device) {
                    echo '*' . $device->mode . '*';
                } else {
                    echo "*id-device-tidak-ditemukan*";
                }
            } else {
                echo "*salah-secret-key*";
            }
        } else {
            echo "*salah-param*";
        }
    }

    public function addCard(Request $request)
    {
        if ($request->key && $request->iddev && $request->rfid) {
            $cekKey = SecretKey::find(1);

            if ($cekKey->key == $request->key) {
                $checkRfid = Rfid::where('rfid', $request->rfid)->first();

                if ($checkRfid) {
                    echo "*RFID-sudah-terdaftar*";
                } else {
                    $device = Device::find($request->iddev);

                    if ($device) {
                        $rfid = Rfid::find(1);

                        $newRfid = $rfid->update([
                            'device_id' => $device->id,
                            'rfid' => $request->rfid
                        ]);

                        if ($newRfid) {
                            $history = History::create([
                                'device_id' => $request->iddev,
                                'rfid' => $request->rfid,
                                'keterangan' => 'ADD RFID CARD'
                            ]);

                            if ($history) {
                                echo "*berhasil-tambah-rfid-card*";
                            } else {
                                echo "*terjadi-kesalahan*";
                            }
                        }
                    } else {
                        echo "*id-device-tidak-ditemukan*";
                    }
                }
            } else {
                echo "*salah-secret-key*";
            }
        } else {
            echo "*salah-param*";
        }
    }

    public function getModeJson(Request $request)
    {
        if ($request->key && $request->iddev) {
            $cekKey = SecretKey::find(1);

            if ($cekKey->key == $request->key) {
                $device = Device::find($request->iddev);

                if ($device) {
                    $response = [
                        'status' => 'success',
                        'mode' => $device->mode,
                        'ket' => 'berhasil'
                    ];
                    echo json_encode($response);
                } else {
                    $response = [
                        'status' => 'warning',
                        'mode' => '-',
                        'ket' => 'id device tidak ditemukan'
                    ];
                    echo json_encode($response);
                }
            } else {
                $response = [
                    'status' => 'failed',
                    'ket' => 'salah secret key'
                ];
                echo json_encode($response);
            }
        } else {
            $response = [
                'status' => 'failed',
                'ket' => 'salah parameter'
            ];
            echo json_encode($response);
        }
    }

    public function addCardJson(Request $request)
    {
        if ($request->key && $request->iddev && $request->rfid) {
            $cekKey = SecretKey::find(1);

            if ($cekKey->key == $request->key) {
                $checkRfid = Rfid::where('rfid', $request->rfid)->first();

                if ($checkRfid) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'RFID sudah terdaftar'
                    ];
                    echo json_encode($response);
                } else {
                    $device = Device::find($request->iddev);

                    if ($device) {
                        $rfid = Rfid::find(1);

                        $newRfid = $rfid->update([
                            'device_id' => $device->id,
                            'rfid' => $request->rfid,
                            'status' => 1
                        ]);

                        if ($newRfid) {
                            $history = History::create([
                                'device_id' => $request->iddev,
                                'rfid' => $request->rfid,
                                'keterangan' => 'ADD RFID CARD'
                            ]);

                            if ($history) {
                                $response = [
                                    'status' => 'success',
                                    'ket' => 'Rfid berhasil ditambahkan'
                                ];
                                echo json_encode($response);
                            } else {
                                $response = [
                                    'status' => 'failed',
                                    'ket' => 'Terjadi Kesalahan'
                                ];
                                echo json_encode($response);
                            }
                        }
                    } else {
                        $response = [
                            'status' => 'failed',
                            'ket' => 'Device tidak ditemukan'
                        ];
                        echo json_encode($response);
                    }
                }
            } else {
                $response = [
                    'status' => 'failed',
                    'ket' => 'salah secret key'
                ];
                echo json_encode($response);
            }
        } else {
            $response = [
                'status' => 'failed',
                'ket' => 'salah param'
            ];
            echo json_encode($response);
        }
    }

    public function kirimWa($rfid, $ket)
    {
        $text = SecretKey::find(5)->key;
        $siswa = Siswa::where('rfid', $rfid->rfid)->first();

        $no_hp = str_split($siswa->no_hp);
        if (str_contains($rfid->no_hp, '08')) {
            if ($no_hp[0] == 0 && $no_hp[1] == 8) {
                unset($no_hp[0]);
                array_unshift($no_hp, '62');
            }
        }
        $no_hp = implode("", $no_hp);

        $message = str_replace('$nama', $siswa->nama, $text);
        $message = str_replace('$kelas', $siswa->kelas->nama, $message);
        $message = str_replace('$keterangan', $ket, $message);
        $message = str_replace('$waktu', Carbon::now()->format('H:i:s'), $message);
        $message = str_replace('$tanggal', Carbon::now()->format('d/m/Y'), $message);

        $url = SecretKey::find(7)->key;

        $data = [
            'api_key' => 'A5ZlWATGqALoLgs1vmkFVTKavAvIbJ',
            'sender' => '089514082888',
            'number' => $no_hp,
            'message' => $message
        ];

        Http::asForm()->post($url, $data);
    }

    public function kirimWaOrtu($rfid, $ket)
    {
        $text = SecretKey::find(5)->key;
        $siswa = Siswa::where('rfid', $rfid->rfid)->first();

        $no_hp_ortu = str_split($siswa->no_hp_ortu);
        if (str_contains($rfid->no_hp_ortu, '08')) {
            if ($no_hp_ortu[0] == 0 && $no_hp_ortu[1] == 8) {
                unset($no_hp_ortu[0]);
                array_unshift($no_hp_ortu, '62');
            }
        }
        $no_hp_ortu = implode("", $no_hp_ortu);

        $message = str_replace('$nama', $siswa->nama, $text);
        $message = str_replace('$kelas', $siswa->kelas->nama, $message);
        $message = str_replace('$keterangan', $ket, $message);
        $message = str_replace('$waktu', Carbon::now()->format('H:i:s'), $message);
        $message = str_replace('$tanggal', Carbon::now()->format('d/m/Y'), $message);

        $url = SecretKey::find(7)->key;
        $data = [
            'api_key' => 'A5ZlWATGqALoLgs1vmkFVTKavAvIbJ',
            'sender' => '089514082888',
            'number' => $no_hp_ortu,
            'message' => $message
        ];

        Http::asForm()->post($url, $data);
    }

    public function kirimWaStaff($rfid, $ket)
    {
        $text = SecretKey::find(6)->key;
        $staff = User::where('rfid', $rfid->rfid)->first();
        $wastaff = SecretKey::find(4);
        $wapimsatu = SecretKey::find(8);
        $wapimdua = SecretKey::find(9);
        $nopimsatu = SecretKey::find(10);
        $nopimdua = SecretKey::find(11);

        $no_hp = str_split($staff->no_hp);
        if (str_contains($rfid->no_hp, '08')) {
            if ($no_hp[0] == 0 && $no_hp[1] == 8) {
                unset($no_hp[0]);
                array_unshift($no_hp, '62');
            }
        }
        $no_hp = implode("", $no_hp);

        $hppimsatu = str_split($nopimsatu->key);
        if (str_contains($nopimsatu->key, '08')) {
            if ($hppimsatu[0] == 0 && $hppimsatu[1] == 8) {
                unset($hppimsatu[0]);
                array_unshift($hppimsatu, '62');
            }
        }
        $hppimsatu = implode("", $hppimsatu);

        $hppimdua = str_split($nopimdua->key);
        if (str_contains($nopimdua->key, '08')) {
            if ($hppimdua[0] == 0 && $hppimdua[1] == 8) {
                unset($hppimdua[0]);
                array_unshift($hppimdua, '62');
            }
        }
        $hppimdua = implode("", $hppimdua);;

        $message = str_replace('$nama', $staff->nama, $text);
        $message = str_replace('$jabatan', $staff->jabatan, $message);
        $message = str_replace('$keterangan', $ket, $message);
        $message = str_replace('$waktu', Carbon::now()->format('H:i:s'), $message);
        $message = str_replace('$tanggal', Carbon::now()->format('d/m/Y'), $message);

        $url = SecretKey::find(7)->key;
        if ($wapimsatu->key == 1) {
            $data = [
                'api_key' => 'A5ZlWATGqALoLgs1vmkFVTKavAvIbJ',
                'sender' => '089514082888',
                'number' => $hppimsatu,
                'message' => $message
            ];

            Http::asForm()->post($url, $data);
        }

        if ($wapimdua->key == 1) {
            $data = [
                'api_key' => 'A5ZlWATGqALoLgs1vmkFVTKavAvIbJ',
                'sender' => '089514082888',
                'number' => $hppimdua,
                'message' => $message
            ];

            Http::asForm()->post($url, $data);
        }

        if ($wastaff->key == 1) {
            $data = [
                'api_key' => 'A5ZlWATGqALoLgs1vmkFVTKavAvIbJ',
                'sender' => '089514082888',
                'number' => $no_hp,
                'message' => $message
            ];

            Http::asForm()->post($url, $data);
        }
    }

    public function absensiJson(Request $request)
    {
        if ($request->key && $request->iddev && $request->rfid) {
            $cekKey = SecretKey::find(1);

            if ($cekKey->key == $request->key) {
                $device = Device::find($request->iddev);

                if ($device) {
                    $rfid = Siswa::where('rfid', $request->rfid)->where('is_active', 1)->first() ?? User::where('rfid', $request->rfid)->where('is_active', 1)->first();
                    if ($rfid) {
                        if ($rfid->status_pelajar == 'Siswa') {

                            $waktu = WaktuOperasional::find(1);
                            $this->absensiSiswa($waktu, $rfid, $device);
                        } else {

                            $waktu = WaktuOperasional::find(2);
                            $this->absenStaff($waktu, $rfid, $device);
                        }
                    } else {
                        $notif = array('status' => 'failed', 'ket' => 'RFID Tidak Ditemukan');
                        echo json_encode($notif);
                    }
                } else {
                    $notif = array('status' => 'failed', 'ket' => 'ID Device Tidak Ditemukan');
                    echo json_encode($notif);
                }
            } else {
                $notif = array('status' => 'failed', 'ket' => 'Salah Secret Key');
                echo json_encode($notif);
            }
        } else {
            $notif = array('status' => 'failed', 'ket' => 'Salah Parameter');
            echo json_encode($notif);
        }
    }

    public function absenStaff($waktu, $rfid, $device)
    {
        $masuk = explode(' - ', $waktu->waktu_masuk);
        $keluar = explode(' - ', $waktu->waktu_keluar);

        $ijin = WaktuOperasional::find(3);

        $startMasuk = Carbon::parse($masuk[0])->subMinute(10)->format('His');
        $endMasuk = Carbon::parse($masuk[1])->format('His');
        $startKeluar = Carbon::parse($keluar[0])->format('His');
        $endKeluar = Carbon::parse($keluar[1])->format('His');
        $telat = Carbon::parse($waktu->telat)->format('His');
        $awalIjin = Carbon::parse($ijin->waktu_masuk)->format('His');
        $akhirIjin = Carbon::parse($ijin->waktu_keluar)->format('His');

        $absen = false;
        $today = Carbon::now()->format('His');

        $now = Carbon::now()->format('Y-m-d 00:00:00');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d 00:00:00');

        if ($today < $startMasuk) {
            $response = [
                'status' => 'failed',
                'ket' => 'Absensi Diluar Waktu'
            ];
            echo json_encode($response);
        }

        if ($today >= $startMasuk && $today <= $endMasuk) {
            $absen = true;
            $ket = "Hadir";
            $status = 1;
            $respon = "Hadir Tepat Waktu";
        }

        if ($today > $endMasuk && $today <= $telat) {
            $absen = true;
            $ket = "Telat Masuk";
            $status = 1;
            $respon = "Telat Masuk";
        }

        if ($today > $telat && $today >= $awalIjin && $today <= $akhirIjin) {
            $absen = true;
            $ket = "Ijin Pulang Awal";
            $status = 1;
            $respon = "Ijin Pulang Awal";
        }

        if ($today >= $startKeluar && $today <= Carbon::parse($endKeluar)->format('His')) {
            $absen = true;
            $ket = "Keluar";
            $status = 1;
            $respon = "Keluar";
        }

        if ($today > Carbon::parse($endKeluar)->format('His')) {
            $absensi = AbsensiStaff::where('user_id', $rfid->id)->where('created_at', '>=', $now)->where('created_at', '<', $tomorrow)->first();

            $response = [
                'status' => 'success',
                'ket' => $absensi->ket,
                'flag' => 'staff',
                'nama' => $rfid->nama,
                'jabatan' => $rfid->jabatan,
                'waktu' => $absensi->waktu_masuk,
                'absensi' => 'Absensi diluar waktu'
            ];

            echo json_encode($response);
        }

        if ($today > Carbon::parse($telat)->format('His') && $today < $startKeluar) {
            $absensi = AbsensiStaff::where('user_id', $rfid->id)->where('created_at', '>=', $now)->where('created_at', '<', $tomorrow)->first();

            $response = [
                'status' => 'success',
                'ket' => $absensi->ket,
                'flag' => 'staff',
                'nama' => $rfid->nama,
                'jabatan' => $rfid->jabatan,
                'waktu' => $absensi->waktu_masuk,
                'absensi' => 'Absensi diluar waktu'
            ];

            echo json_encode($response);
        }

        if ($absen) {
            $absensi = AbsensiStaff::where('user_id', $rfid->id)->where('created_at', '>=', $now)->where('created_at', '<', $tomorrow)->first();


            if (!$absensi && $today <= $telat) {
                try {
                    AbsensiStaff::create([
                        'device_id' => $device->id,
                        'user_id' => $rfid->id,
                        'masuk' =>  $status ?? 0,
                        'waktu_masuk' => Carbon::now()->format('Y-m-d H:i:s'),
                        'status_hadir' => $ket,
                        'ket' => $ket,
                    ]);

                    History::create([
                        'device_id' => $device->id,
                        'rfid' => $rfid->rfid,
                        'keterangan' => $ket
                    ]);

                    $response = [
                        'status' => 'success',
                        'ket' => $respon,
                        'flag' => 'staff',
                        'nama' => $rfid->nama,
                        'jabatan' => $rfid->jabatan,
                        'waktu' => date('d/m/Y H:i:s'),
                        'absensi' => 'Masuk'
                    ];


                    $this->kirimWaStaff($rfid, $ket);

                    echo json_encode($response);
                } catch (\Throwable $th) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'gagal insert absensi'
                    ];
                    echo json_encode($response);
                }
            } else if ($absensi && $today >= $telat && $today <= $startKeluar) {
                $response = array('status' => 'failed', 'ket' => 'Absensi diluar waktu operasional');
                echo json_encode($response);
            } else if ($absensi && $absensi->masuk == 1 && $absensi->keluar == 0 && $today >= $telat && Carbon::now('Asia/Jakarta')->format('His') >= $awalIjin && Carbon::now('Asia/Jakarta')->format('His') <= $akhirIjin) {
                try {
                    $absensi->update([
                        'keluar' => 1,
                        'waktu_keluar' => Carbon::now()->format('Y-m-d H:i:s'),
                        'ket' => 'Ijin Pulang Awal',
                    ]);

                    History::create([
                        'device_id' => $device->id,
                        'rfid' => $rfid->rfid,
                        'keterangan' => $ket
                    ]);

                    $response = [
                        'status' => 'success',
                        'ket' => 'Ijin Pulang Awal',
                        'flag' => 'staff',
                        'nama' => $rfid->nama,
                        'jabatan' => $rfid->jabatan,
                        'waktu' => date('d/m/Y H:i:s'),
                        'absensi' => 'Ijin Pulang Awal'
                    ];

                    $this->kirimWaStaff($rfid, $ket);
                    echo json_encode($response);
                } catch (\Throwable $th) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'gagal insert absensi'
                    ];
                    echo json_encode($response);
                }
            } else if ($absensi && $absensi->masuk == 1 && Carbon::now('Asia/Jakarta')->format('His') < $startKeluar) {
                // $response = [
                //     'status' => 'failed',
                //     'ket' => 'Sudah Absensi'
                // ];

                $response = [
                    'status' => 'success',
                    'ket' => $absensi->ket,
                    'flag' => 'staff',
                    'nama' => $rfid->nama,
                    'jabatan' => $rfid->jabatan,
                    'waktu' => $absensi->waktu_masuk,
                    'absensi' => 'Sudah Absensi'
                ];

                echo json_encode($response);
            } else if ($absensi && $absensi->masuk == 1 && $absensi->keluar == 0 && Carbon::now('Asia/Jakarta')->format('His') >= $startKeluar) {
                try {
                    $absensi->update([
                        'keluar' => $ket == 'Keluar' ? $status : 0,
                        'waktu_keluar' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);

                    History::create([
                        'device_id' => $device->id,
                        'rfid' => $rfid->rfid,
                        'keterangan' => $ket
                    ]);

                    $response = [
                        'status' => 'success',
                        'ket' => $respon,
                        'flag' => 'staff',
                        'nama' => $rfid->nama,
                        'jabatan' => $rfid->jabatan,
                        'waktu' => date('d/m/Y H:i:s'),
                        'absensi' => 'Keluar'
                    ];

                    $this->kirimWaStaff($rfid, $ket);
                    echo json_encode($response);
                } catch (\Throwable $th) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'Gagal Insert Absensi'
                    ];
                    echo json_encode($response);
                }
            } else {
                $response = array('status' => 'failed', 'ket' => 'Sudah Absensi');
                echo json_encode($response);
            }
        }
    }

    public function absensiSiswa($waktu, $rfid, $device)
    {
        $masuk = explode(' - ', $waktu->waktu_masuk);
        $keluar = explode(' - ', $waktu->waktu_keluar);

        $ijin = WaktuOperasional::find(3);

        $startMasuk = Carbon::parse($masuk[0])->format('His');
        $endMasuk = Carbon::parse($masuk[1])->format('His');
        $startKeluar = Carbon::parse($keluar[0])->format('His');
        $endKeluar = Carbon::parse($keluar[1])->format('His');
        $telat = Carbon::parse($waktu->telat)->format('His');
        $awalIjin = Carbon::parse($ijin->waktu_masuk)->format('His');
        $akhirIjin = Carbon::parse($ijin->waktu_keluar)->format('His');

        $wasiswa = SecretKey::find(2);
        $waortu = SecretKey::find(3);

        $absen = false;
        $today = Carbon::now()->format('His');
        $hari = Carbon::now()->format('Y-m-d');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d');

        if ($today < $startMasuk) {
            $response = [
                'status' => 'failed',
                'ket' => 'Absensi Diluar Waktu'
            ];
            echo json_encode($response);
        }

        if ($today >= $startMasuk && $today <= $endMasuk) {
            $absen = true;
            $ket = "Hadir";
            $status = 1;
            $respon = "Hadir Tepat Waktu";
        }

        if ($today > $endMasuk && $today <= $telat) {
            $absen = true;
            $ket = "Telat Masuk";
            $status = 1;
            $respon = "Telat Masuk";
        }

        if ($today > $telat && $today >= $awalIjin && $today <= $akhirIjin) {
            $absen = true;
            $ket = "Ijin Pulang Awal";
            $status = 1;
            $respon = "Ijin Pulang Awal";
        }

        if ($today > Carbon::parse($telat)->format('His') && $today < $startKeluar) {
            $absensi = Absensi::where('siswa_id', $rfid->id)->where('created_at', '>=', $hari)->where('created_at', '<', $tomorrow)->first();

            $response = [
                'status' => 'success',
                'ket' => $absensi->ket,
                'flag' => 'siswa',
                'nama' => $absensi->siswa->nama,
                'waktu' => $absensi->waktu_masuk,
                'absensi' => 'Absensi diluar waktu'
            ];
            echo json_encode($response);
        }

        if ($today >= $startKeluar && $today <= Carbon::parse($endKeluar)->format('His')) {
            $absen = true;
            $ket = "Keluar";
            $status = 1;
            $respon = "Keluar";
        }

        if ($today > Carbon::parse($endKeluar)->format('His')) {
            $absensi = Absensi::where('siswa_id', $rfid->id)->where('created_at', '>=', $hari)->where('created_at', '<', $tomorrow)->first();

            $response = [
                'status' => 'success',
                'ket' => $absensi->ket,
                'flag' => 'siswa',
                'nama' => $absensi->siswa->nama,
                'waktu' => $absensi->waktu_masuk,
                'absensi' => 'Absensi diluar waktu'
            ];
            echo json_encode($response);
        }

        if ($absen) {
            $absensi = Absensi::where('siswa_id', $rfid->id)->where('created_at', '>=', $hari)->where('created_at', '<', $tomorrow)->first();


            if (!$absensi && $today <= $telat) {
                try {
                    Absensi::create([
                        'device_id' => $device->id,
                        'siswa_id' => $rfid->id,
                        'masuk' => $status ?? 0,
                        'waktu_masuk' => Carbon::now()->format('Y-m-d H:i:s'),
                        'status_hadir' => $ket,
                        'ket' => $ket
                    ]);

                    History::create([
                        'device_id' => $device->id,
                        'rfid' => $rfid->rfid,
                        'keterangan' => $ket
                    ]);

                    $response = [
                        'status' => 'success',
                        'ket' => $respon,
                        'flag' => 'siswa',
                        'nama' => $rfid->nama,
                        'kelas' => $rfid->kelas->nama,
                        'waktu' => date('d/m/Y H:i:s'),
                        'absensi' => 'Masuk'
                    ];

                    if ($wasiswa->key == 1) {
                        $this->kirimWa($rfid, $ket);
                    }

                    if ($waortu->key == 1) {
                        $this->kirimWaOrtu($rfid, $ket);
                    }
                    echo json_encode($response);
                } catch (\Throwable $th) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'gagal insert absensi'
                    ];
                    echo json_encode($th->getMessage());
                }
            } else if ($absensi && $absensi->masuk == 1 && $absensi->keluar == 0 && $today < $startKeluar && $today > $telat) {
                // $response = [
                //     'status' => 'failed',
                //     'ket' => 'Sudah Absensi'
                // ];

                $response = [
                    'status' => 'success',
                    'ket' => $absensi->ket,
                    'flag' => 'siswa',
                    'nama' => $absensi->siswa->nama,
                    'waktu' => $absensi->waktu_masuk,
                    'absensi' => 'Sudah Absensi'
                ];

                echo json_encode($response);
            } else if ($absensi && $absensi->masuk == 1 && $absensi->keluar == 0 && $today >= $telat && $today >= $awalIjin && $today <= $akhirIjin) {
                try {
                    $absensi->update([
                        'keluar' => 1,
                        'waktu_keluar' => Carbon::now()->format('Y-m-d H:i:s'),
                        'ket' => 'Ijin Pulang Awal',
                    ]);

                    History::create([
                        'device_id' => $device->id,
                        'rfid' => $rfid->rfid,
                        'keterangan' => $ket
                    ]);

                    $response = [
                        'status' => 'success',
                        'ket' => 'Ijin Pulang Awal',
                        'flag' => 'staff',
                        'nama' => $rfid->nama,
                        'jabatan' => $rfid->jabatan,
                        'waktu' => date('d/m/Y H:i:s'),
                        'absensi' => 'Ijin Pulang Awal'
                    ];

                    if ($wasiswa->key == 1) {
                        $this->kirimWa($rfid, $ket);
                    }

                    if ($waortu->key == 1) {
                        $this->kirimWaOrtu($rfid, $ket);
                    }

                    echo json_encode($response);
                } catch (\Throwable $th) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'gagal insert absensi'
                    ];
                    echo json_encode($response);
                }
            } else if ($absensi && $absensi->masuk == 1 && $absensi->keluar == 0 && $today >= $startKeluar && $today > $telat) {
                try {
                    $absensi->update([
                        'keluar' => $ket == 'Keluar' ? $status : 0,
                        'waktu_keluar' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);

                    History::create([
                        'device_id' => $device->id,
                        'rfid' => $rfid->rfid,
                        'keterangan' => $ket
                    ]);


                    $response = [
                        'status' => 'success',
                        'ket' => $respon,
                        'flag' => 'siswa',
                        'nama' => $rfid->nama,
                        'kelas' => $rfid->kelas->nama,
                        'waktu' => date('d/m/Y H:i:s'),
                        'absensi' => 'Keluar'
                    ];

                    if ($wasiswa->key == 1) {
                        $this->kirimWa($rfid, $ket);
                    }

                    if ($waortu->key == 1) {
                        $this->kirimWaOrtu($rfid, $ket);
                    }
                    echo json_encode($response);
                } catch (\Throwable $th) {
                    $response = [
                        'status' => 'failed',
                        'ket' => 'Gagal Insert Absensi'
                    ];
                    echo json_encode($response);
                }
            } else {
                $response = array('status' => 'failed', 'ket' => 'Sudah Absensi');
                echo json_encode($response);
            }
        }
    }
}
