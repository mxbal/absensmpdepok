<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Kelas;
use App\Models\Pesan;
use App\Models\Siswa;
use App\Models\Absensi;
use App\Models\SecretKey;
use Illuminate\Support\Str;
use App\Models\AbsensiStaff;
use Illuminate\Http\Request;
use App\Models\WaktuOperasional;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index()
    {
        $totalUser = User::where('id', '!=', 1)->where('is_active', 1)->count();
        $totalSiswa = Siswa::where('is_active', 1)->count();
        $totalKelas = Kelas::count();
        $totalPengguna = $totalUser + $totalSiswa;

        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d 00:00:00');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d 00:00:00');

        $hadirStaff = AbsensiStaff::where('status_hadir', 'Hadir')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $zoomStaff = AbsensiStaff::where('status_hadir', 'Hadir Via Zoom')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $sakitStaff = AbsensiStaff::where('status_hadir', 'Sakit')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $ijinStaff = AbsensiStaff::where('status_hadir', 'Ijin')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $alpaStaff = AbsensiStaff::where('status_hadir', 'Alpa')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();

        $hadirSiswa = Absensi::where('status_hadir', 'Hadir')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $zoomSiswa = Absensi::where('status_hadir', 'Hadir Via Zoom')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();

        $sakitSiswa = Absensi::where('status_hadir', 'Sakit')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $ijinSiswa = Absensi::where('status_hadir', 'Ijin')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();
        $alpaSiswa = Absensi::where('status_hadir', 'Alpa')->where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->count();

        $siswa = Absensi::where('created_at', '>=', $now)->where('created_at', '<=', $tomorrow)->where('masuk', 0)->where('keluar', 0)->get();

        return view('dashboard.index', compact('totalUser', 'totalSiswa', 'totalKelas', 'totalPengguna', 'hadirStaff', 'zoomStaff', 'sakitStaff', 'ijinStaff', 'alpaStaff', 'hadirSiswa', 'zoomSiswa', 'sakitSiswa', 'ijinSiswa', 'alpaSiswa', 'siswa'));
    }

    public function setting()
    {
        auth()->user()->can('setting-access') ? true : abort(403);

        $secretKey = SecretKey::find(1);
        $wasiswa = SecretKey::find(2);
        $waortu = SecretKey::find(3);
        $wastaff = SecretKey::find(4);
        $pesansiswa = SecretKey::find(5);
        $pesanstaff = SecretKey::find(6);
        $url = SecretKey::find(7);
        $wapimsatu = SecretKey::find(8);
        $wapimdua = SecretKey::find(9);
        $nopimsatu = SecretKey::find(10);
        $nopimdua = SecretKey::find(11);

        $waktu = WaktuOperasional::find(1);
        $waktuStaff = WaktuOperasional::find(2);
        $ijinPulang = WaktuOperasional::find(3);

        $masuk = explode(' - ', $waktu->waktu_masuk);
        $keluar = explode(' - ', $waktu->waktu_keluar);
        $masukStaff = explode(' - ', $waktuStaff->waktu_masuk);
        $keluarStaff = explode(' - ', $waktuStaff->waktu_keluar);

        $pesan = SecretKey::find(2);
        return view('dashboard.setting', compact('waktu', 'waktuStaff', 'ijinPulang', 'masuk', 'keluar', 'masukStaff', 'keluarStaff', 'secretKey', 'wasiswa', 'waortu', 'wastaff', 'pesansiswa', 'pesanstaff', 'url', 'wapimsatu', 'wapimdua', 'nopimsatu', 'nopimdua'));
    }

    public function updateWaktu(Request $request, $id)
    {

        $request->validate([
            'waktu_awal_masuk' => 'required',
            'waktu_akhir_masuk' => 'required',
            'waktu_awal_keluar' => 'required',
            'waktu_akhir_keluar' => 'required',
            'waktu_awal_masuk_staff' => 'required',
            'waktu_akhir_masuk_staff' => 'required',
            'waktu_awal_keluar_staff' => 'required',
            'waktu_akhir_keluar_staff' => 'required',
            'telat_siswa' => 'required',
            'telat_staff' => 'required',
            'waktu_awal_ijin' => 'required',
            'waktu_akhir_ijin' => 'required',
        ]);

        try {
            DB::beginTransaction();
            // Waktu Masuk Siswa
            $waktuOperasional = WaktuOperasional::find($id);

            $masuk = $request->waktu_awal_masuk . ' - ' . $request->waktu_akhir_masuk;
            $keluar = $request->waktu_awal_keluar . ' - ' . $request->waktu_akhir_keluar;

            $waktuOperasional->update([
                'waktu_masuk' => $masuk,
                'waktu_keluar' => $keluar,
                'telat' => $request->telat_siswa
            ]);

            // Waktu Masuk Staff (Mon - Thu)
            $weekday = WaktuOperasional::find(2);

            $masukWeek = $request->waktu_awal_masuk_staff . ' - ' . $request->waktu_akhir_masuk_staff;
            $keluarWeek = $request->waktu_awal_keluar_staff . ' - ' . $request->waktu_akhir_keluar_staff;

            $weekday->update([
                'waktu_masuk' => $masukWeek,
                'waktu_keluar' => $keluarWeek,
                'telat' => $request->telat_staff
            ]);;

            // Waktu ijin Pulang
            $ijin = WaktuOperasional::find(3);

            $ijin->update([
                'waktu_masuk' => $request->waktu_awal_ijin,
                'waktu_keluar' => $request->waktu_akhir_ijin,
            ]);

            DB::commit();

            return redirect()->route('setting')->with('success', 'Waktu Operasional berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('setting')->with('error', $th->getMessage());
        }
    }

    public function profile()
    {
        return view('dashboard.profile');
    }

    public function update(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'no_hp' => 'required',
            'gender' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $user = User::find(auth()->user()->id);

            if ($request->password) {
                $password = bcrypt($request->password);
            } else {
                $password = auth()->user()->password;
            }

            if ($request->file('foto')) {
                Storage::delete($user->foto);
                $foto = $request->file('foto');
                $fotoUrl = $foto->storeAs('images/user', date('YmdHis') . '-' . Str::slug($request->nama) . '.' . $foto->extension());
            } else {
                $fotoUrl = $user->foto;
            }


            $user->update([
                'nama' => $request->nama,
                'no_hp' => $request->no_hp,
                'gender' => $request->gender,
                'password' => $password,
                'foto' => $fotoUrl,
            ]);

            DB::commit();
            return back()->with('success', 'Profile berhasil diupdate');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function updatePesan(Request $request)
    {

        try {
            DB::beginTransaction();
            $pesanSiswa = SecretKey::find($request->pesansiswa_id);

            $pesanSiswa->update([
                'key' => $request->pesan_siswa,
            ]);

            $pesanStaff = SecretKey::find($request->pesanstaff_id);

            $pesanStaff->update([
                'key' => $request->pesan_staff,
            ]);

            $nopimsatu = SecretKey::find(10);

            $nopimsatu->update([
                'key' => $request->no_pimpinan_1,
            ]);

            $nopimdua = SecretKey::find(11);

            $nopimdua->update([
                'key' => $request->no_pimpinan_2,
            ]);

            $url = SecretKey::find($request->url_id);

            $url->update([
                'key' => $request->url,
            ]);


            DB::commit();
            return back()->with('success', 'Pesan berhasil diupdate');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function status()
    {
        try {
            DB::beginTransaction();
            $wa = SecretKey::find(request('id'));

            $wa->update(['key' => request('status')]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => request('status') == 1 ? 'Notifikasi wa berhasil diaktifkan' : 'Notifikasi wa berhasil dinonaktifkan'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage()
            ]);
        }
    }
}
