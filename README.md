Installation

Clone this project using 
$ git clone https://gitlab.com/mxbal/absensmpdepok.git

Run composer install
$ composer install

Copy .env file
$ cp .env.example .env

Setting your database in .env file

Generate the key
$ php artisan key:generate

Migration the database and seeder
$ php artisan migrate --seed

Run the project
$ php artisan serve